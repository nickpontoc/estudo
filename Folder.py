import os

class Folder(object):
    """class que permite a facilitar o processamento dos arquivos em determina-
    das pastas, serve unicamente para aprender."""

    def __init__(self, path = './',): #função inicializadora da classe
        self.path = path

    def setFolder(self, path): #seleciona a pasta na qual será mostrado os docs
        self.path = path;

    def listDocuments(self): #imprime na tela todos os arquivos disponíveis napasta
        documents = os.listdir(self.path)
        for document in documents:
            print(document)
